
var division;
let cuatro=4;

//Creacion de funciones flecha con dos y un paremetro

const suma = (numero1,numero2) => {return numero1+numero2;};
const multiplicacion = numero => numero*100;  

//Función anonima con uso de una variable global

division = function (numero){return numero/cuatro;};

//Uso de funciones tipo callback

function llamarCallback(cuadrado){
    cuadrado();  
};
function cuadrado(){
    let cuadradoDos;
    cuadradoDos = cuatro*cuatro;
    console.log("El numero 4 elevado al cuadrado es: "+cuadradoDos);
};


